#!/bin/bash

set -euo pipefail

set +e
if ! git diff-files --quiet --ignore-submodules ; then
  echo "Uncommited changes; stash or commit before deploying"
  exit 1
fi
if ! git diff-index --cached --quiet HEAD --ignore-submodules ; then
  echo "Staged but uncommited changes; commit before deploying"
  exit 2
fi
set -e

DATE=$(date +"%Y-%m-%d_%H%M%S")
lein uberjar
scp target/metaslurp.jar flexdb:/www/deploys/metaslurp/metaslurp-${DATE}.jar
git tag "${DATE}"
ssh flexdb "cd /www/deploys/metaslurp/ && ./run.sh 'metaslurp-${DATE}.jar'"
