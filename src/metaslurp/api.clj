(ns metaslurp.api
  (:require [compojure.core :refer [defroutes OPTIONS GET]]
            [compojure.handler :refer [api]]
            [ring.util.response :as resp]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [metaslurp.core :refer [page-info]]
            [org.httpkit.server :refer [run-server]])
  (:gen-class))

(defn generate-edn-response
  "Generate a response with content-type edn"
  [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/edn; charset=utf-8"
             "Access-Control-Allow-Origin" "*"}
   :body (pr-str data)})

(defn generate-json-response
  "Generate a json response"
  [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/json; charset=utf-8"
             "Access-Control-Allow-Origin" "*"}
   :body (json/write-str data)})

(defn response-for
  [req data & [status]]
  (let [accepts (string/split (get-in req [:headers "accept"] "") #",")]
    (if (some #(.startsWith % "application/edn") accepts)
      (generate-edn-response data status)
      (generate-json-response data status))))

(defroutes api-routes
  (OPTIONS "/*" req
    (-> (resp/response "")
        (assoc-in [:headers "Access-Control-Allow-Origin"] "*")
        (assoc-in
          [:headers "Access-Control-Allow-Headers"]
          (get-in req [:headers "access-control-request-headers"]))
        (assoc-in
          [:headers "Access-Control-Allow-Methods"]
          (get-in req [:headers "access-control-request-method"]))))
  (GET "/" req
    (try
      (response-for req (page-info (get-in req [:query-params "url"])))
      (catch java.io.IOException _
        {:status 400
         :headers {"Content-Type" "text/plain"
                   "Access-Control-Allow-Origin" "*"}
         :body "Couldn't load url"}))))

(defonce server (atom nil))

(defn stop-server
  []
  (when-not (nil? @server)
    (println "Stopping server")
    (@server :timeout 100)
    (reset! server nil)))

(defn -main
  [& [port-num & _]]
  (let [port (if port-num
               (try
                 (Integer. port-num)
                 (catch NumberFormatException _
                   4000))
               4000)]
    (println "Starting on port " port)
    (reset! server (run-server (api #'api-routes) {:port port}))))
