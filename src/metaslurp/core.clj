(ns metaslurp.core
  (:require [clojure.string :as string]
            [net.cgrand.enlive-html :as h]
            [metaslurp.utils :refer [read-url flip]])
  (:import [org.apache.pdfbox.pdmodel PDDocument]))

(defn extract-opengraph-info
  "Extract the opengraph information from a page's HTML content"
  [content]
  (-> content
      (h/select [:head [:meta (h/attr-starts :property "og:")]])
      (->> (map (comp (juxt (comp keyword (flip string/replace #"^og:" "") :property) :content) :attrs))
           (into {}))))

(defn extract-html-info
  "Extract information from an HTML document, preferring OpenGraph and falling
  back to heuristics if unavailable."
  [content url]
  (let [page (h/html-resource content)
        info (-> page
                 extract-opengraph-info
                 (update-in [:title]
                            #(if (some? %)
                               %
                               (first (h/select page [:head :title h/content]))))
                 (update-in [:description]
                            #(if (some? %)
                               %
                               (-> (h/select page [:head [:meta (h/attr= :name "description")]])
                                   first
                                   (get-in [:attrs :content])))))]
    (if (and (:image info) (not (re-matches #"http(s)://.*$" (:image info))))
      (dissoc info :image)
      info)))

(defn extract-image-info
  "Get the dimensions of the image"
  [content url]
  (let [image (javax.imageio.ImageIO/read content)]
    {:image url
     :type "image"
     :width (.getWidth image)
     :height (.getHeight image)}))

(defn extract-pdf-info
  "Get as much informations as we can about a given pdf"
  [content url]
  (with-open [pd (PDDocument/load content)]
    (let [info (.getDocumentInformation pd)]
      {:type "PDF"
       :title (.getTitle info)
       :author (.getAuthor info)
       :description (or (.getSubject info)
                        (.getKeywords info))})))

(defn page-info
  "Get meta information about a given URL."
  [^String url]
  (let [[content content-type] (read-url url)]
    (condp re-matches content-type
      #"image/.*$"  (extract-image-info content url)
      #"application/pdf" (extract-pdf-info content url)
      #"text/html.*" (extract-html-info content url)
      {})))
