(ns metaslurp.utils)

(defn read-url
  [url]
  (-> url
      (java.net.URL.)
      (.openConnection)
      (doto
        (.addRequestProperty
          "User-Agent" (str "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) "
                            "AppleWebKit/537.75.14 (KHTML, like Gecko) "
                            "Version/7.0.3 Safari/537.75.14"))
        (.connect))
      (->> ((juxt #(.getInputStream %) #(.getContentType %))))))

(defn flip
  "Partially apply the function f to the given args, which will come after the
  next args.  i.e. ((flip vector 3 4) 1 2) => [1 2 3 4]"
  [f & a]
  (fn [& b]
    (apply f (concat b a))))
