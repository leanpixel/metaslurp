# metaslurp

A Clojure library designed to grab the metadata for a particular page, using
OpenGraph if it can.

## Usage

Get the latest version from [clojars][]:

```
[metaslurp "0.2.3"]
```

### Using the library:

```clojure
user=> (require '[metaslurp.core :refer [page-info]])

user=> (pprint (page-info "http://vimeo.com/95066828"))
{:description "James Mickens' session from Monitorama PDX 2014.",
 :video "http://vimeo.com/moogaloop.swf?clip_id=95066828",
 :video:height "360",
 :type "video",
 :video:width "640",
 :title "Monitorama PDX 2014 - James Mickens",
 :site_name "Vimeo",
 :url "http://vimeo.com/95066828",
 :video:type "application/x-shockwave-flash",
 :video:secure_url "https://vimeo.com/moogaloop.swf?clip_id=95066828"}


user=> (pprint (page-info "http://31.media.tumblr.com/avatar_9d44be2e5bd1_128.png"))
{:image "http://31.media.tumblr.com/avatar_9d44be2e5bd1_128.png",
 :type "image",
 :width 128,
 :height 128}
```

### Running the server

You can also run the service as a stand-alone API server that returns
information in either JSON or EDN.

Start the server running, either in a repl

```clojure
user=> (require '[metaslurp.api :refer [-main]])

user=> (-main 4001)
```

or using leiningen

```bash
$ lein run 4001
```

Then you can query the endpoint by passing a `?url=` query parameter.

```bash
$ curl -H 'Accept: application/edn' localhost:4001/?url=https://github.com/jamesnvc
{:site_name "GitHub", :type "profile", :image "https://avatars0.githubusercontent.com/u/38405?s=400", :title "jamesnvc (James Cash)", :url "https://github.com/jamesnvc", :description "jamesnvc has 42 repositories written in JavaScript, Python, and VimL. Follow their code on GitHub."}


$ curl localhost:4001/?url=http://thenextfounders.ca
{"title":"Scale your business and your mindset @ The Next Founders","type":"website","url":"http:\/\/www.thenextfounders.ca\/","description":"The Next 36, Canada\u2019s most selective program for young entrepreneurs is seeking 20 high potential founders of promising tech ventures, to join the 2014 cohort of The Next Founders. The Next Founders is an education program, designed for innovators, that involves an extraordinary community of business leaders, entrepreneurs, professors and investors.","site_name":"The Next Founders","video":"http:\/\/player.vimeo.com\/video\/85566045"}
```

## License

Copyright © 2014 Lean Pixel

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.

  [clojars]: https://clojars.org/metaslurp
